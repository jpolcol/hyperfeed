#!/usr/bin/python
# TODO Be more specific on exception error messages
# TODO Decide what actions to take on exceptions (kill the parser? warn the user? break?)
# TODO If a feed isn't responding fast, bump it down to the bottom of the feed list and allow the others to produce
# TODO Make warning message if parser is running -> "it's not recommended to make changes while the parser is running, but you can..."
# TODO Add more state checks
import urllib2
import os
import sys
import feedparser
import codecs
import time
from subprocess import Popen, PIPE

# TODO Re-evaluate this
def check_internet_connect():
    while True:
        try:
            urllib2.urlopen("http://www.google.com")
        except urllib2.URLError, e:
            print("[DEBUG: TIMEOUT -> No internet connection]")
            time.sleep(25)
        else:
            break

def source_settings():
    hyperfeed = os.getenv("HOME") + '/' + ".hyperfeed"

    try:
        f = codecs.open(hyperfeed + '/' + "settings", 'r', "utf-8")
    except IOError as e:
        sys.exit("ERROR: settings file does not exist. Please run HyperFeed to automatically generate it.")

    lines = []

    # Prepend "export" on each line
    # Settings will now source correctly and be accessible to this script
    for line in f.readlines():
        lines.append("export " + line)

    f.close()

    f = codecs.open("/tmp/parser_settings", 'w', "utf-8")
    f.writelines(lines)
    f.close()

    pipe = Popen("source /tmp/parser_settings; env", stdout=PIPE, shell=True)
    data = pipe.communicate()[0]

    env = dict((line.split('=', 1) for line in data.splitlines()))

    os.environ.update(env)

def get_parser_state():
    while True:
        try:
            f = codecs.open("/tmp/parser_state", 'r', "utf-8")
            break
        except IOError as e:
            f = codecs.open("/tmp/parser_state", 'w', "utf-8")
            f.write('')
            f.close()

    state = f.readline().rstrip()
    f.close()

    f = codecs.open("/tmp/parser_state", 'w', "utf-8")
    f.write('')
    f.close()

    return state

if __name__ == '__main__':
    hyperfeed = os.getenv("HOME") + '/' + ".hyperfeed"
    tree = hyperfeed + '/' + "tree"

    source_settings()

    # Validate and format settings
    # Variable: save_all_read_titles
    if os.getenv("SAVE_ALL_READ_TITLES") is None:
        sys.exit("ERROR: settings file does not specify variable -> SAVE_ALL_READ_TITLES")

    if os.getenv("SAVE_ALL_READ_TITLES").upper() == "FALSE":
        save_all_read_titles = False
    elif os.getenv("SAVE_ALL_READ_TITLES").upper() == "TRUE":
        save_all_read_titles = True
    else:
        sys.exit("ERROR: settings file variable \"SAVE_ALL_READ_TITLES\" must be either \"true\" or \"false\"")

    # Variable: parser_refresh_interval
    if os.getenv("PARSER_REFRESH_INTERVAL") is None:
        sys.exit("ERROR: settings file does not specify variable -> PARSER_REFRESH_INTERVAL")

    try:
        parser_refresh_interval = int(os.getenv("PARSER_REFRESH_INTERVAL"))
    except ValueError:
        sys.exit("ERROR: settings file variable \"PARSER_REFRESH_INTERVAL\" must be an integer")

    # Main parser loop
    while True:
        check_internet_connect()

        while True:
            try:
                f = codecs.open(hyperfeed + '/' + "feeds", 'r', "utf-8")
                break
            except IOError as e:
                print("[DEBUG: FEEDS FILE NOT FOUND -> CREATING AS EMPTY FILE]")

                f = codecs.open(hyperfeed + '/' + "feeds", 'w', "utf-8")
                f.write('')
                f.close()

        feeds = f.readlines()
        f.close()

        print("[DEBUG: PARSING STARTED]")

        for feed in feeds:
            feed_line = ''
            feed_lines = []

            description, url = feed.split(";;")

            path = description.rpartition('/')[0]
            title = description.rpartition('/')[2]

            if not os.path.exists(tree + '/' + path):
                os.makedirs(tree + '/' + path)

            try:
                p = feedparser.parse(url)
            except:
                # TODO Put an X next to the feed name
                print("[DEBUG: TIMEOUT -> " + url.rstrip()  + ']')
                continue

            try:
                if p.feed.has_key("title") and p.feed.has_key("link"):
                    for entry in p.entries:
                        feed_line = entry.title + ";;" + entry.link + '\n'
                        feed_lines.append(feed_line)
            except:
                print("[DEBUG: Could not parse -> " + title + ']')
                continue

            # Handle dotfile
            try:
                dotfile = codecs.open(tree + '/' + path + '/' + '.' + title, 'r', "utf-8")
                dotfile_lines = dotfile.readlines()
                dotfile.close()

                dotfile_exists = True
            except IOError as e:
                dotfile_exists = False

            if dotfile_exists:
                new_feed_lines = ''
                new_dotfile_lines = ''

                for feed_line in feed_lines:
                    add = True

                    for dotfile_line in dotfile_lines:
                        if feed_line == dotfile_line:
                            add = False

                    if add:
                        new_feed_lines += feed_line
                    else:
                        new_dotfile_lines += feed_line

                try:
                    tree_feed = codecs.open(tree + '/' + path + '/' + title, 'r', "utf-8")
                except IOError as e:
                    print("[DEBUG: ERROR -> FAILED TO OPEN \"" + title + "\" FOR READING]")
                    continue

                tree_feed_lines = tree_feed.read()
                tree_feed.close()

                if tree_feed_lines == new_feed_lines:
                    print("[DEBUG: SKIPPED (EVEN WITH DOTFILE) -> " + title + ']')
                    continue
                else:
                    if not save_all_read_titles:
                        if new_dotfile_lines:
                            try:
                                dotfile = codecs.open(tree + '/' + path + '/' + '.' + title, 'w', "utf-8")
                            except IOError as e:
                                sys.exit("ERROR: dotfile does not exist for this feed.")

                            dotfile.writelines(new_dotfile_lines)
                            dotfile.close()
                        else:
                            os.remove(tree + '/' + path + '/' + '.' + title)
                            print("[DEBUG: DELETED -> " + tree + '/' + path + '/' + '.' + title + ']')

                    if new_feed_lines:
                        try:
                            tree_feed = codecs.open(tree + '/' + path + '/' + title, 'w', "utf-8")
                        except IOError as e:
                            sys.exit("ERROR: the file for this feed does not exist in the tree.")

                        tree_feed.writelines(new_feed_lines)
                        tree_feed.close()

                        print("[DEBUG: UPDATED AND MODIFIED -> " + title + ']')

                        continue
            # No dotfile
            else:
                try:
                    tree_feed = codecs.open(tree + '/' + path + '/' + title, 'r', "utf-8")
                except IOError as e:
                    print("[DEBUG: ERROR -> FAILED TO OPEN " + title + " FOR READING]")
                    continue

                tree_feed_lines = tree_feed.readlines()
                tree_feed.close()

                # Handle feed comparison
                if tree_feed_lines == feed_lines:
                    print("[DEBUG: SKIPPED -> " + title + ']')
                    continue

                # Tree feed lines differs from parsed rss lines, so update
                try:
                    tree_feed = codecs.open(tree + '/' + path + '/' + title, 'w', "utf-8")
                except IOError as e:
                    sys.exit("ERROR: the file for this feed (" + title + ") does not exist in the tree.")

                tree_feed.writelines(feed_lines)
                tree_feed.close()

            print("[DEBUG: UPDATED -> " + title + ']')

	print("[DEBUG: PARSING ENDED]")

        time.sleep(parser_refresh_interval)
